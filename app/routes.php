<?php
declare(strict_types=1);

use App\Application\Actions\PreflightAction;
use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Firebase\JWT\JWT;
use Tuupola\Base62;






function get_token($user){

	$now = new DateTime();
	$future = new DateTime("now +2 hours");
	$jti = (new Base62)->encode(random_bytes(16));

	$secret = "your_secret_key";

	$payload = [
	    "jti" => $jti,
	    "iat" => $now->getTimeStamp(),
	    "nbf" => 1357000000,
	    "data" => $user //array
	];

	$token = JWT::encode($payload, $secret, "HS256");
	return $token;
	}

function decodejwt($jwt){
    $secretKey = "your_secret_key";
    $token = JWT::decode($jwt, $secretKey, array('HS256'));
    return json_encode($token);
	}

function is_auth(Request $request) {
    	$headers = $request->getHeader("Authorization");
    	$headers = str_replace("Bearer ", "", json_encode($headers[0]));
    	$headers = trim($headers, '"');

    	$decoded = json_decode(decodejwt($headers));
    	
    	if( $decoded->data->customer_group_id  == "7" )
    		return true;
    	else
	    	return false;
    }

function getCatByPro(Request $request, $id) {

        $ch = curl_init();
        $url = "https://irresistibles.cl/index.php?route=rest/categories/getCatByPro";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "product_id=".$id);
         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec ($ch));
         
        curl_close ($ch);
        //print_r($data);
        return $data;
    }	

//guardar_producto_carrito
function agregar_producto_carrito_actual($user_id, $product_id, $cantidad){
	try{
			$db = new db();
			$connection = $db->getConnection();
			
			$sql = "SELECT COUNT(product_id) as cont FROM copec_product_cart where product_id = ".$product_id. " AND user_id = ".$user_id." AND session_id = 0";
			$dbh = $connection->prepare($sql);
			$dbh->execute();
			$user = $dbh->fetch(PDO::FETCH_ASSOC);

			

			
			if ($user["cont"]  == 0){
				$sql = "INSERT INTO copec_product_cart(product_id, cantidad_product_id, user_id, session_id) VALUES(".$product_id. ", " .$cantidad.", ".$user_id.", 0)";
				$dbh = $connection->prepare($sql);
				$dbh->execute();

				$data = json_encode(array('status' => 'OK', 'message' => 'insertado' ));
				//$response->getBody()->write($data);
				//return $response
				 //         ->withHeader('Content-Type', 'application/json')
				 //         ->withStatus(200);

				}
			else{

				$sql = "UPDATE copec_product_cart SET cantidad_product_id = cantidad_product_id + ".$cantidad." WHERE product_id = ".$product_id." AND user_id = ".$user_id." AND session_id =0";
				$dbh = $connection->prepare($sql);
				$dbh->execute();

				$data = json_encode(array('status' => 'OK', 'message' => 'modificado' ));
				//$response->getBody()->write($data);
				//return $response
				//          ->withHeader('Content-Type', 'application/json')
				//          ->withStatus(200);


				$data = json_encode(array('status' => 'error', 'message' => 'Login failed' ));
				//$response->getBody()->write($data);
				//return $response
				 //         ->withHeader('Content-Type', 'application/json')
				//          ->withStatus(401);
				}
			
			}
	catch(PDOException $e){
		$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

		//$response->getBody()->write($data);
		//	return $response
		//	    ->withHeader('Content-Type', 'application/json')
		//		->withStatus(401);
		}
}


return function (App $app) {

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

	

	
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });


	


	$app->post("/apiapp/login", function(Request $request, Response $response){
		$params = $request->getParsedBody();

		$username = $params['username'];
		$password = $params['password'];

		try{
			$db = new db();
			$connection = $db->getConnection();
			
			$sql = "SELECT customer_id, customer_group_id, firstname, password, salt FROM oc_customer where firstname = '".$username. "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" .$password. "'))))) OR password = '" .md5($password) . "') AND status = '1'";
			


			$dbh = $connection->prepare($sql);
			$dbh->execute();
			$user = $dbh->fetch(PDO::FETCH_ASSOC);


			$connection = null;
			

			if ($user == false){
				$data = json_encode(array('status' => 'error', 'message' => 'Login failed' ));
				$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);

				}
			else{
				$data = json_encode(array('message' => 'Successful login.', 'token' => get_token($user), 'expireAt' => (new DateTime("now +2 hours"))->getTimeStamp() ));
				$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(200);
				}
			}
		catch(PDOException $e){
			$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

			$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);
			}
	});

    $app->group('/users', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT customer_id, customer_group_id, firstname FROM oc_customer";
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT customer_id, customer_group_id, fisrstname FROM oc_customer WHERE customer_id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});

    $app->group('/categorias', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM categorias where estado = 1";
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM categorias WHERE id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});

    $app->group('/apiapp/nombres_cortos', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_nombre_corto where estado = 1";
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_nombre_corto WHERE id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});

    $app->group('/apiapp/aplicaciones', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_aplicaciones where estado = 1 and id <> 0";
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_aplicaciones WHERE id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});

    $app->group('/apiapp/capacitaciones', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_capacitacion where estado > 0";
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_capacitacion WHERE id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});


    $app->group('/apiapp/productos', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				//$sql = "SELECT * FROM productos_copec where activo = 1";

				$sql = "SELECT p.*, app.nombre as nombre_aplicacion, nc.nombre as nombre_corto, e.nombre as nombre_envase ";
				
				$sql .= "FROM copec_productos p ";
				
				$sql .= "INNER JOIN copec_aplicaciones app ON p.id_aplicacion = app.id ";
				$sql .= "INNER JOIN copec_nombre_corto nc ON p.id_nombre_corto = nc.id ";
				$sql .= "INNER JOIN copec_envases e ON p.id_envase = e.id ";

				//$orden = $app->request()->get('orden');
				//if ( $orden ){
				//	if ( $orden == "nombre_corto"){
						$sql .= " GROUP BY nombre_corto";
				//	}
				//}

				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);

				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_productos WHERE id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});

    $app->group('/apiapp/lista_productos', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$total =  array();

				$sql0 = " SELECT DISTINCT id_nombre_corto as id, tipo, nc.nombre from copec_productos p ";
				$sql0 .= "INNER JOIN copec_nombre_corto nc ON p.id_nombre_corto = nc.id WHERE tipo = 1";
				$dbh0 = $connection->prepare($sql0);
				$dbh0->execute();
				$ncortos = $dbh0->fetchAll(PDO::FETCH_ASSOC);
				foreach ($ncortos as $n) {
					$aux = array();

					$sql = "SELECT id, id_nombre_corto, nombre, imagen ";
					$sql .= "FROM copec_productos p ";
					$sql .= " WHERE id_nombre_corto = ".$n["id"];

					$dbh = $connection->prepare($sql);
					$dbh->execute();
					
					$aux["nombre_corto"] = $n["nombre"];
					$aux["datos"] = $dbh->fetchAll(PDO::FETCH_ASSOC);;

					array_push($total, $aux);
					
				}


				$connection = null;
				
				$data = json_encode($total);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        });

    $app->group('/apiapp/lista_promociones', function (Group $group) {
        $group->get('', function(Request $request, Response $response){
        if (is_auth($request) == true)
			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$total =  array();

				$sql0 = "SELECT id, nombre, tipo, imagen from copec_productos WHERE tipo = 2";
				$dbh0 = $connection->prepare($sql0);
				$dbh0->execute();
				$total = $dbh0->fetchAll(PDO::FETCH_ASSOC);

				$connection = null;
				
				$data = json_encode($total);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
		else{
				$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
			}
	        });
        });

    $app->group('/apiapp/lista_productos_aplicacion', function (Group $group) {
        $group->get('/{id}', function(Request $request, Response $response, $args){
        	if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_productos WHERE id_aplicacion = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}
	    	});
    	});

    $app->group('/apiapp/getcarritos', function (Group $group) {
        $group->get('/{id}', function(Request $request, Response $response, $args){

	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_cart WHERE user_id = ". $args['id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}

	    	});
    	});


    $app->post('/apiapp/guardar_producto_carrito',  function(Request $request, Response $response) {
		$params = $request->getParsedBody();

		$user_id = $params['user_id'];
		$product_id = $params['product_id'];
		$cantidad = $params['cantidad'];

		try{
			$db = new db();
			$connection = $db->getConnection();
			
			$sql = "SELECT COUNT(*) as cont FROM copec_product_cart where product_id = ".$product_id. " AND user_id = ".$user_id." AND session_id = 0";
			$dbh = $connection->prepare($sql);
			$dbh->execute();
			$user = $dbh->fetch(PDO::FETCH_ASSOC);

			

			
			if ($user["cont"]  == 0){
				$sql = "INSERT INTO copec_product_cart(product_id, cantidad_product_id, user_id, session_id) VALUES(".$product_id. ", " .$cantidad.", ".$user_id.", 0)";
				$dbh = $connection->prepare($sql);
				$dbh->execute();

				$data = json_encode(array('status' => 'OK', 'message' => 'insertado' ));
				$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(200);

				}
			else{

				$sql = "UPDATE copec_product_cart SET cantidad_product_id = cantidad_product_id + ".$cantidad." WHERE product_id = ".$product_id." AND user_id = ".$user_id." AND session_id =0";
				$dbh = $connection->prepare($sql);
				$dbh->execute();

				$data = json_encode(array('status' => 'OK', 'message' => 'modificado' ));
				$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(200);


				$data = json_encode(array('status' => 'error', 'message' => 'Login failed' ));
				$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);
				}
			
			}
		catch(PDOException $e){
			$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

			$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);
			}
        });

    $app->post('/apiapp/update_producto_carrito',  function(Request $request, Response $response) {
		$params = $request->getParsedBody();

		$user_id = $params['user_id'];
		$product_id = $params['product_id'];
		$cantidad = $params['cantidad'];

		try{
			$db = new db();
			$connection = $db->getConnection();


				$sql = "UPDATE copec_product_cart SET cantidad_product_id = ".$cantidad." WHERE product_id = ".$product_id." AND user_id = ".$user_id." AND session_id =0";
				$dbh = $connection->prepare($sql);
				$dbh->execute();

				$data = json_encode(array('status' => 'OK', 'message' => 'modificado' ));
				$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(200);
			
			}
		catch(PDOException $e){
			$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

			$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);
			}
        });

	$app->post('/apiapp/agregar_al_carrito_actual',  function(Request $request, Response $response) {
			
			$params = $request->getParsedBody();
			$user_id = $params['user_id'];
			$id_carrito = $params['id_carrito'];

			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * from copec_product_cart WHERE session_id = ".$id_carrito;
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				
				$productos = $dbh->fetchAll(PDO::FETCH_ASSOC);

				//$data = $dbh->fetchAll(PDO::FETCH_ASSOC);
				


		        foreach ($productos as $item) {
		        	agregar_producto_carrito_actual($user_id, $item["product_id"], $item["cantidad_product_id"]);

	            //$sentence = "IF EXISTS(SELECT product_id, cantidad_product_id FROM copec_product_cart WHERE user_id=".$user_id." and session_id=0 and product_id = ". $item->product_id.") ";
	            //$sentence .= "THEN UPDATE copec_product_cart SET  SET cantidad_product_id = cantidad_product_id + ".$item->cantidad_product_id." WHERE user_id=".$user_id." and session_id=0 and product_id = ". $item->product_id."; ";
	            //$sentence .= "ELSE INSERT INTO copec_product_cart(product_id, cantidad_product_id, user_id, session_id) VALUES(".$product_id. ", " .$item->cantidad_product_id.", ".$user_id.", 0)";

	            //$dbh = $connection->prepare($sentence);
	            //$dbh->execute();
    	        }

    	        $cont_sql = "Select Count(product_id) as cnt FROM copec_product_cart WHERE user_id=".$user_id." and session_id=0";
				$dbh = $connection->prepare($cont_sql);
				$dbh->execute();
				$total = $dbh->fetch(PDO::FETCH_ASSOC);

				$d = json_encode(array('status' => 'OK', 'message' => $total["cnt"] ));
				$response->getBody()->write($d);
				return $response
					->withHeader('Content-Type', 'application/json')
				   	->withStatus(200);
				
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => $sql, 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
	        });

	$app->post('/apiapp/delete_producto_carrito',  function(Request $request, Response $response) {
			
			$params = $request->getParsedBody();
			$order_id = $params['order_id'];

			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "DELETE from copec_product_cart WHERE orden_id = ".$order_id;
				$dbh = $connection->prepare($sql);
				$dbh->execute();

				$data = json_encode(array('status' => 'OK', 'message' => $order_id ));
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
				   	->withStatus(200);
				
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => $sql, 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
	        });

    $app->get('/apiapp/get_nro_prod/{user_id}',  function(Request $request, Response $response, $args) {
		$params = $request->getParsedBody();

		

		try{
			$db = new db();
			$connection = $db->getConnection();


			$sql = "SELECT COUNT(*) as cont FROM copec_product_cart where user_id = ".$args['user_id']." AND session_id = 0";
			$dbh = $connection->prepare($sql);
			$dbh->execute();
			$user = $dbh->fetch(PDO::FETCH_ASSOC);
			
			$data = json_encode($user);
			$response->getBody()->write($data);

			return $response
				->withHeader('Content-Type', 'application/json')
				->withStatus(200);
			
			}
		catch(PDOException $e){
			$data = json_encode(array('status' => 'error', 'message' => $e->getMessage(), 'sql' => $sql));

			$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);
			}
        });
        
    $app->group('/apiapp/carrito_actual', function (Group $group) {
	    $group->post('',  function(Request $request, Response $response) {
			$params = $request->getParsedBody();

			$user_id = $params['id'];

			try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT MAX(session_id) as max FROM copec_product_cart";
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetch(PDO::FETCH_ASSOC);
				$max = $user["max"] + 1;
				
				
					$sql = "UPDATE copec_product_cart SET session_id = ".$max." WHERE user_id = ".$user_id." AND session_id = 0";
					$dbh = $connection->prepare($sql);
					$dbh->execute();

					$sql = "INSERT INTO copec_cart(id, nombre, user_id) VALUES ( ".$max.", 'Pedido ".$max."', ".$user_id.")";
					$dbh = $connection->prepare($sql);
					$dbh->execute();


					$data = json_encode(array('status' => 'OK', 'message' => $sql ));
					$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(200);
				
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => $sql, 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
	        });

    	});


    	//});
    $app->group('/apiapp/carrito_actual', function (Group $group) {
        $group->get('/{user_id}/{cart_id}', function(Request $request, Response $response, $args){
        	//if (is_auth($request) == true){
	        try{
				$db = new db();
				$connection = $db->getConnection();
				
				$sql = "SELECT * FROM copec_product_cart INNER JOIN copec_productos ON copec_product_cart.product_id = copec_productos.id WHERE user_id = ".$args['user_id']." AND session_id = ".$args['cart_id'];
				$dbh = $connection->prepare($sql);
				$dbh->execute();
				$user = $dbh->fetchAll(PDO::FETCH_ASSOC);


				$connection = null;
				
				$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
				}
			catch(PDOException $e){
				$data = json_encode(array('status' => 'error', 'message' => $e->getMessage()));

				$response->getBody()->write($data);
					return $response
					          ->withHeader('Content-Type', 'application/json')
					          ->withStatus(401);
				}
			/*}
			else{
					$data = json_encode(array('status' => 'error', 'message' => 'No estas autorizado'));

					$response->getBody()->write($data);
						return $response
						          ->withHeader('Content-Type', 'application/json')
						          ->withStatus(401);
				}*/
	    	});



    });


    $app->get('/apiapp/catalog',  function(Request $request, Response $response, $args) {

        $ch = curl_init();
        $url = "https://irresistibles.cl/index.php/?route=rest/categories/get";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "group_id=39");
         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec ($ch));
         
        curl_close ($ch);
        $categorias_limpias = [];

        foreach ($data->categories as $item) {
            $aux = [];
            $aux["category_id"] = $item->category_id;
            $aux["name"] = $item->name;
            $aux["productos"] = [];
            array_push($categorias_limpias, $aux);
        }


        $ch = curl_init();
        $url = "https://irresistibles.cl/index.php/?route=rest/products/all";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "group_id=39");
         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec ($ch));
        
        curl_close ($ch);        
        $new_array = [];
        
        foreach($data as $key => $value) {
            array_push($new_array, $value);
        }


        $arreglo_id_productos = array_column($new_array, 'product_id');
        foreach ($arreglo_id_productos as $id_productos) {
        
            $catpr = getCatByPro($request, $id_productos);

            foreach($catpr as $item){
            	$item_categ = array_search($item, array_column($categorias_limpias, 'category_id'));
                $item_p = $new_array[array_search($id_productos, array_column($new_array, 'product_id'))];
                array_push($categorias_limpias[$item_categ]["productos"], $item_p);
            }
        }

		$response->getBody()->write(json_encode($categorias_limpias));

		return $response
			->withHeader('Content-Type', 'application/json')
			->withStatus(200);
    });
       

    $app->get('/apiapp/catalog/{id}',  function(Request $request, Response $response, $args) {

        $ch = curl_init();
        $url = "https://irresistibles.cl/index.php/?route=rest/products/get";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "group_id=39&product_id=".$args["id"]);
         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec ($ch);
         
        curl_close ($ch);
        //print_r($data);
        //print($data);
        //return $args["id"];
		//$data = json_encode($user);
				$response->getBody()->write($data);
				return $response
					->withHeader('Content-Type', 'application/json')
					->withStatus(200);
		
    });

	$app->group('/apiapp/puntos', function (Group $group) {
        $group->get('/{user_id}', function(Request $request, Response $response, $args){

		$params = $request->getParsedBody();


		try{
			$db = new db();
			$connection = $db->getConnection();


			$sql = "SELECT SUM(points) as cnt FROM oc_customer_reward where customer_id = ".$args['user_id'];
			$dbh = $connection->prepare($sql);
			$dbh->execute();
			$user = $dbh->fetch(PDO::FETCH_ASSOC);
			
			$data = json_encode($user);
			$response->getBody()->write($data);

			return $response
				->withHeader('Content-Type', 'application/json')
				->withStatus(200);
			
			}
		catch(PDOException $e){
			$data = json_encode(array('status' => 'error', 'message' => $e->getMessage(), 'sql' => $sql));

			$response->getBody()->write($data);
				return $response
				          ->withHeader('Content-Type', 'application/json')
				          ->withStatus(401);
			}
        
	        });
        });

    $app->post('/apiapp/canjear',  function(Request $request, Response $response) {
		
		$params = $request->getParsedBody();

		$user_id = $params['user_id'];
		$product_id = $params['product_id'];
		$cantidad = $params['cantidad'];
		$precio = $params['precio'];

		$sql = "SELECT SUM(points) as cnt FROM oc_customer_reward where customer_id = ".$user_id;
		
		$db = new db();
		$connection = $db->getConnection();

		$dbh = $connection->prepare($sql);
		$dbh->execute();
		$user = $dbh->fetch(PDO::FETCH_ASSOC);
		
		$puntos = $user["cnt"];
		$descont = $cantidad * $precio;
		if( $descont > $puntos){
			$data = json_encode(array('status' => 'error', 'message' => 'No dispones de sufucuientes puntos'));
			$response->getBody()->write($data);

			return $response
				->withHeader('Content-Type', 'application/json')
				->withStatus(200);
		}
		else{

			$sql = "INSERT INTO oc_customer_reward(customer_id, points, description, date_added) VALUES(".$user_id.", -".$descont.", '".$cantidad." del producto ".$product_id."', '".date('Y-m-d')."' )";
			$dbh = $connection->prepare($sql);
			$dbh->execute();

			$data = json_encode(array('status' => 'OK', 'message' => 'Felicidades, has cajeado este producto'));
			$response->getBody()->write($data);

			return $response
				->withHeader('Content-Type', 'application/json')
				->withStatus(200);			
		}
        });

};
